import { Component, OnInit } from '@angular/core';
import { UserService } from '../user.service';
// import { Subscriber } from 'rxjs';
// import { HttpResponse } from '@angular/common/http';
// import { ClientResponse, ServerResponse } from 'http';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
  providers: [UserService]
})
export class HomeComponent implements OnInit {
  register;
  firstName: string = 'Tom';
  lastName: string = 'Hopkins';
  gender: string = 'Male';
  age: number = 20;
  number : number = 8967766405;
  boolean : boolean = true;
  column : number =2;
  email: string = 'pampa7208@gmail.com';
  onClick() : void{
    console.log(this.firstName);
    console.log("hi friends fffffffffff");
  }
  showHide : boolean = false;
  showDetails() : void{
        this.showHide = !this.showHide;
  }
  employees : any[];
 
showHiddentableDetails : boolean = false;
tableDetails():void{
  this.showHiddentableDetails = !this.showHiddentableDetails;
}

getEmployeeDetails():void{
  this.employees = [
      { code: 'emp100', name: 'Pom', gender: 'Female', annualSalary: 7500, dateOfBirth: Date.now() },
      { code: 'emp101', name: 'Tom', gender: 'Male', annualSalary: 5500, dateOfBirth: Date.now() },
      { code: 'emp102', name: 'Alex', gender: 'Male', annualSalary: 5700.95, dateOfBirth: Date.now() },
      { code: 'emp103', name: 'Mike', gender: 'Male', annualSalary: 5900, dateOfBirth: Date.now() },
      { code: 'emp104', name: 'Mary', gender: 'Female', annualSalary: 6500.826, dateOfBirth: Date.now() },
      { code: 'emp105', name: 'Mary', gender: 'Female', annualSalary: 6500.826, dateOfBirth: Date.now() },
  ];
}
  // constructor() { 
  //   this.employees = [
  //     { code: 'emp101', name: 'Tom', gender: 'Male', annualSalary: 5500, dateOfBirth:'07/30/1999'  },
  //     { code: 'emp102', name: 'Alex', gender: 'Male', annualSalary: 5700.95, dateOfBirth: '07/30/1999' },
  //     { code: 'emp103', name: 'Mike', gender: 'Male', annualSalary: 5900, dateOfBirth: '07/30/1999' },
  //     { code: 'emp104', name: 'Mary', gender: 'Female', annualSalary: 6500.826, dateOfBirth: '07/30/1999' },
  // ];
  // }
constructor(private userService:UserService){
  this.employees = [
        { code: 'emp101', name: 'Tom', gender: 'Male', annualSalary: 5500, dateOfBirth:'07/30/1999'  },
        { code: 'emp102', name: 'Alex', gender: 'Male', annualSalary: 5700.95, dateOfBirth: '07/30/1999' },
        { code: 'emp103', name: 'Mike', gender: 'Male', annualSalary: 5900, dateOfBirth: '07/30/1999' },
        { code: 'emp104', name: 'Mary', gender: 'Female', annualSalary: 6500.826, dateOfBirth: '07/30/1999' },
    ];
}
  ngOnInit() {
    this.register = {
      name : '',
      mobileno : '',
      department: ''
    };
  }
  registerUser(){
    console.log('hiiiiiiiiiii')
     this.userService.registerNewUser(this.register).subscribe(
       Response =>{         
         console.log(Response)
         alert('has been created') 
         this.register = Response;
       },
       error => console.log('error',error)
     );
  }
  getUser(){
    this.userService.getNewUser(this.register).subscribe(
      backend =>{
        console.log(backend) 
        this.register = backend;
      },
      error => console.log('error',error)
    );
  }
}
